package com.tao.wsa.googleauth.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.services.plus.Plus;
import com.google.api.services.plus.model.PeopleFeed;
import com.google.gson.Gson;
import com.tao.wsa.googleauth.main.ClientSecrets;

/**
 * Get list of people user has shared with this app.
 */
public class PeopleServlet extends HttpServlet {

	private static final Gson GSON = new Gson();

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json");

		// Only fetch a list of people for connected users.
		String tokenData = (String) request.getSession().getAttribute("token");
		if (tokenData == null) {
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			response.getWriter().print(GSON.toJson("Current user not connected."));
			return;
		}
		try {
			// Build credential from stored token data.
			GoogleCredential credential = new GoogleCredential.Builder().setJsonFactory(ClientSecrets.JSON_FACTORY)
					.setTransport(ClientSecrets.TRANSPORT).setClientSecrets(ClientSecrets.CLIENT_ID, ClientSecrets.CLIENT_SECRET).build()
					.setFromTokenResponse(ClientSecrets.JSON_FACTORY.fromString(tokenData, GoogleTokenResponse.class));
			// Create a new authorized API client.
			Plus service = new Plus.Builder(ClientSecrets.TRANSPORT, ClientSecrets.JSON_FACTORY, credential).setApplicationName(ClientSecrets.APPLICATION_NAME)
					.build();
			// Get a list of people that this user has shared with this app.
			PeopleFeed people = service.people().list("me", "visible").execute();
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().print(GSON.toJson(people));
		} catch (IOException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().print(GSON.toJson("Failed to read data from Google. " + e.getMessage()));
		}
	}
}