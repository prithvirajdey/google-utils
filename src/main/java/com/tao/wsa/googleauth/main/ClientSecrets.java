package com.tao.wsa.googleauth.main;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson.JacksonFactory;

public class ClientSecrets {

	

	/*
	 * Optionally replace this with your application's name.
	 */
	public static final String APPLICATION_NAME = "Google+ Login Implementation";

	/*
	 * Default HTTP transport to use to make HTTP requests.
	 */
	public static final HttpTransport TRANSPORT = new NetHttpTransport();

	public static final JacksonFactory JSON_FACTORY = new JacksonFactory();

	public static GoogleClientSecrets clientSecrets;
	
	/*
	 * This is the Client ID that you generated in the API Console.
	 */
	public static String CLIENT_ID;

	/*
	 * This is the Client Secret that you generated in the API Console.
	 */
	public static String CLIENT_SECRET;

	static {
		try {

			System.out.println(System.getProperty("user.dir"));
			String filePath = System.getProperty("user.dir") + "/src/main/resources/";
			System.out.println(filePath);
			Reader reader = new FileReader(filePath + "client_secrets.json");
			clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, reader);
			CLIENT_ID = clientSecrets.getWeb().getClientId();
			CLIENT_SECRET = clientSecrets.getWeb().getClientSecret();
		} catch (IOException e) {
			throw new Error("No client_secrets.json found", e);
		}
	}

}
