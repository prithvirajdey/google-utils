/*
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tao.wsa.googleauth.main;

import org.apache.log4j.BasicConfigurator;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.servlet.ServletHandler;
import org.mortbay.jetty.servlet.SessionHandler;

import com.tao.wsa.googleauth.servlet.ConnectServlet;
import com.tao.wsa.googleauth.servlet.DisconnectServlet;
import com.tao.wsa.googleauth.servlet.MainServlet;
import com.tao.wsa.googleauth.servlet.PeopleServlet;

/**
 * Simple server to demonstrate how to use Google+ Sign-In and make a request
 * via your own server.
 */
public class Signin {

	/**
	 * Register all endpoints that we'll handle in our server.
	 * 
	 * @param args
	 *            Command-line arguments.
	 * @throws Exception
	 *             from Jetty if the component fails to start
	 */
	public static void main(String[] args) throws Exception {
		BasicConfigurator.configure();
		Server server = new Server(4567);
		ServletHandler servletHandler = new ServletHandler();
		SessionHandler sessionHandler = new SessionHandler();
		sessionHandler.setHandler(servletHandler);
		server.setHandler(sessionHandler);
		servletHandler.addServletWithMapping(ConnectServlet.class, "/connect");
		servletHandler.addServletWithMapping(DisconnectServlet.class, "/disconnect");
		servletHandler.addServletWithMapping(PeopleServlet.class, "/people");
		servletHandler.addServletWithMapping(MainServlet.class, "/");
		server.start();
		server.join();
	}

}
